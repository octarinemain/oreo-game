'use strict';

$(document).ready(function () {

    // Cookies
    function cookies() {
        var cookies = $('.cookies');
        var data = sessionStorage.getItem('cookie');
        if (data === null) {
            cookies.addClass('js-show');
        }

        $('.cookies .btn').on('click', function (e) {
            e.preventDefault();
            cookies.removeClass('js-show');
            sessionStorage.setItem('cookie', 'ok');
        });
    }

    cookies();

    // Clamping Footer
    function clampingFooter() {
        var footerW = $('.footer').outerHeight();
        $('.height-helper').css('margin-top', '-' + footerW + 'px');
        $('.content-wrap').css('padding-top', footerW + 'px');
    }

    clampingFooter();

    // PopUp
    function popUp() {
        $('.js-popup-button').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });

        // Close PopUp
        $('.js-close-popup').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function (e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }

    popUp();

    function regSwiper() {
        var swiper = new Swiper('.swiper-reg', {
            speed: 400,
            effect: "fade",
            simulateTouch: false,
            pagination: {
                el: '.swiper-pagination'
            }
        });

        var validator = $(".registration__form").validate();

        $('.js-slide-next').on('click', function (e) {
            e.preventDefault();

            var inputClass = '.' + $(this).closest('.registration__card').find('input').attr('name');
            var answer = validator.element(inputClass);

            if (answer) {
                swiper.slideNext();
            }
        });
    }

    regSwiper();

    function game() {
        $('.biscuit__card').on('click', function () {
            $(this).addClass('active-popup');
            $(this).mouseleave('handlerOut', function () {
                $(this).removeClass('active-popup');
            });
        });

        $('.js-more-biscuit').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.biscuit__card').addClass('js-dis').removeClass('active-popup');
            setTimeout(function () {
                $('.biscuit__card').removeClass('js-dis');
            }, 1000);
        });

        $('.biscuit__card:nth-child(4n + 1)').addClass('position1');
        $('.biscuit__card:nth-child(2n)').addClass('position2');

        $('.js-open-biscuit').on('click', function (e) {
            e.preventDefault();
            var thisParent = $(this).closest('.biscuit__card');
            if (thisParent.hasClass('position1')) {
                thisParent.addClass('open open-anim').find('.biscuit__anim').attr('src', 'img/position1.gif');
            } else if (thisParent.hasClass('position2')) {
                thisParent.addClass('open open-anim').find('.biscuit__anim').attr('src', 'img/position2.gif');
            } else {
                thisParent.addClass('open open-anim').find('.biscuit__anim').attr('src', 'img/position3.gif');
            }
            setTimeout(function () {
                thisParent.find('.biscuit__anim').attr('src', '');
                thisParent.removeClass('open-anim');
            }, 1600);
        });
    }

    game();

    // Burger
    function burger() {
        $('.header__burger').on('click', function () {
            $('.header__nav').toggleClass('active');
        });
    }

    burger();
});